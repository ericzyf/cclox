#include "lox.hpp"
#include <iostream>

namespace lox {

void report(unsigned line, std::string_view where, std::string_view message) {
  std::cerr << "[line " << line << "] Error" << where << ": " << message
            << std::endl;
}

void error(unsigned line, std::string_view message) {
  report(line, "", message);
}

void error(const Token& token, std::string_view message) {
  if (token.type() == TokenType::EndOfFile) {
    report(token.line(), " at end", message);
  } else {
    std::string where(" at '");
    where += to_str(token.type());
    switch (token.type()) {
      case lox::TokenType::Identifier:
      case lox::TokenType::String:
        where += '(';
        where += token.get_str();
        where += ')';
        break;

      case lox::TokenType::Number:
        where += '(';
        where += std::to_string(token.get_number());
        where += ')';
        break;

      default:
        break;
    }
    where += '\'';

    report(token.line(), where, message);
  }
}

}  // namespace lox