#include "lexer.hpp"
#include <cassert>
#include <charconv>
#include "lox.hpp"

namespace {

bool is_digit(char c) {
  return c >= '0' && c <= '9';
}

bool is_alpha(char c) {
  return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_';
}

bool is_alphanum(char c) {
  return is_alpha(c) || is_digit(c);
}

struct KeywordTokenType {
  std::string_view keyword;
  lox::TokenType token_type;
};

constexpr KeywordTokenType KEYWORDS[] = {
    {"and", lox::TokenType::And},       {"class", lox::TokenType::Class},
    {"else", lox::TokenType::Else},     {"false", lox::TokenType::False},
    {"for", lox::TokenType::For},       {"fun", lox::TokenType::Fun},
    {"if", lox::TokenType::If},         {"nil", lox::TokenType::Nil},
    {"or", lox::TokenType::Or},         {"print", lox::TokenType::Print},
    {"return", lox::TokenType::Return}, {"super", lox::TokenType::Super},
    {"this", lox::TokenType::This},     {"true", lox::TokenType::True},
    {"var", lox::TokenType::Var},       {"while", lox::TokenType::While}};

}  // namespace

namespace lox {

Lexer::Lexer(std::string_view s)
    : source_(s), start_(0), current_(0), line_(1), error_count_(0) {}

#define emit(e) \
  return { TokenType::e, line_ }

Token Lexer::next_token() {
BEGIN:
  if (at_end()) {
    emit(EndOfFile);
  }

  start_ = current_;
  const char c = advance();
  switch (c) {
    case '(':
      emit(LeftParen);
    case ')':
      emit(RightParen);
    case '{':
      emit(LeftBrace);
    case '}':
      emit(RightBrace);
    case ',':
      emit(Comma);
    case '.':
      emit(Dot);
    case '-':
      emit(Minus);
    case '+':
      emit(Plus);
    case ';':
      emit(Semicolon);
    case '*':
      emit(Star);

    case '!':
      if (match('=')) {
        emit(BangEqual);
      }
      emit(Bang);
    case '=':
      if (match('=')) {
        emit(EqualEqual);
      }
      emit(Equal);
    case '<':
      if (match('=')) {
        emit(LessEqual);
      }
      emit(Less);
    case '>':
      if (match('=')) {
        emit(GreaterEqual);
      }
      emit(Greater);

    case '/':
      if (match('/')) {
        line_comment();
        goto BEGIN;
      } else {
        emit(Slash);
      }

    case ' ':
    case '\r':
    case '\t':
      // Ignore whitespace.
      goto BEGIN;

    case '\n':
      ++line_;
      goto BEGIN;

    case '"':
      return string_literal();

    default:
      if (is_digit(c)) {
        if (auto n = number_literal()) {
          return std::move(*n);
        }
      } else if (is_alpha(c)) {
        return identifier();
      }

      error(line_, "Unexpected character.");
      ++error_count_;
      goto BEGIN;
  }
}
#undef emit

unsigned Lexer::error_count() const {
  return error_count_;
}

std::vector<Token> Lexer::tokenize(std::string_view s, unsigned& error_count) {
  std::vector<Token> tokens;
  Lexer l(s);
  while (tokens.emplace_back(l.next_token()).type() != TokenType::EndOfFile)
    ;
  // The last token should be EOF.
  assert(tokens.back().type() == TokenType::EndOfFile);

  error_count = l.error_count();
  return tokens;
}

bool Lexer::at_end() const {
  return current_ >= source_.size();
}

char Lexer::peek() const {
  assert(!at_end());
  return source_[current_];
}

char Lexer::peek_next() const {
  const auto idx = current_ + 1;
  return idx < source_.size() ? source_[idx] : '\0';
}

char Lexer::advance() {
  assert(!at_end());
  return source_[current_++];
}

bool Lexer::match(char expected) {
  if (!at_end() && peek() == expected) {
    ++current_;
    return true;
  }
  return false;
}

// Keep consuming chars until LF or EOF.
void Lexer::line_comment() {
  while (!at_end() && peek() != '\n') {
    ++current_;
  }
}

Token Lexer::string_literal() {
  for (; !at_end(); ++current_) {
    if (peek() == '"') {
      // Trim the surrounding quotes.
      assert(source_[start_] == '"');
      ++start_;
      const std::string_view str(&source_[start_], current_ - start_);

      // Consume the closing quote.
      ++current_;
      return Token::of_string(str, line_);
    }
    if (peek() == '\n') {
      ++line_;
    }
  }

  // Closing quote not found.
  error(line_, "Unterminated string.");
  ++error_count_;
  return {TokenType::EndOfFile, line_};
}

std::optional<Token> Lexer::number_literal() {
  while (!at_end()) {
    if (is_digit(peek())) {
      ++current_;
    } else {
      // match "\.[0-9]+"
      if (peek() == '.' && is_digit(peek_next())) {
        current_ += 2;
        while (!at_end() && is_digit(peek())) {
          ++current_;
        }
      }
      break;
    }
  }

  // Unfortunately, there's a great chance that the floating-point version of
  // std::from_chars is unsupported in the C++ stdlib even if the compiler
  // itself supports C++17. So use std::stod here to allow compilation with
  // older C++17 compilers...
  try {
    const auto number = std::stod({&source_[start_], current_ - start_});
    return Token::of_number(number, line_);
  } catch (...) {
    return std::nullopt;
  }
}

Token Lexer::identifier() {
  while (!at_end() && is_alphanum(peek())) {
    ++current_;
  }

  const std::string_view ident(&source_[start_], current_ - start_);
  for (const auto& kw : KEYWORDS) {
    if (ident == kw.keyword) {
      return {kw.token_type, line_};
    }
  }
  return Token::of_identifier(ident, line_);
}

}  // namespace lox