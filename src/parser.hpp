#pragma once
#include <vector>
#include "expr.hpp"

namespace lox {

class Parser {
 public:
  explicit Parser(const std::vector<Token>&);
  std::unique_ptr<Expr> parse();

 private:
  std::vector<Token>::const_iterator current_;

  bool at_end() const;
  const Token& peek() const;
  const Token* match(TokenType);
  const Token* match(std::initializer_list<TokenType>);
  void throw_error(std::string_view);
  void synchronize();

  std::unique_ptr<Expr> expression();
  std::unique_ptr<Expr> equality();
  std::unique_ptr<Expr> comparison();
  std::unique_ptr<Expr> term();
  std::unique_ptr<Expr> factor();
  std::unique_ptr<Expr> unary();
  std::unique_ptr<Expr> primary();
};

}  // namespace lox