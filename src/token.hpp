#pragma once
#include <iostream>
#include <memory>

namespace lox {

enum class TokenType : uint8_t {
  // Single-character tokens.
  LeftParen,
  RightParen,
  LeftBrace,
  RightBrace,
  Comma,
  Dot,
  Minus,
  Plus,
  Semicolon,
  Slash,
  Star,

  // One or two character tokens.
  Bang,
  BangEqual,
  Equal,
  EqualEqual,
  Greater,
  GreaterEqual,
  Less,
  LessEqual,

  // Literals.
  Identifier,
  String,
  Number,

  // Keywords.
  And,
  Class,
  Else,
  False,
  Fun,
  For,
  If,
  Nil,
  Or,
  Print,
  Return,
  Super,
  This,
  True,
  Var,
  While,

  EndOfFile
};

#define LOX_TOK_STR(e) \
  case TokenType::e:   \
    return #e;

constexpr std::string_view to_str(TokenType t) {
  switch (t) {
    LOX_TOK_STR(LeftParen)
    LOX_TOK_STR(RightParen)
    LOX_TOK_STR(LeftBrace)
    LOX_TOK_STR(RightBrace)
    LOX_TOK_STR(Comma)
    LOX_TOK_STR(Dot)
    LOX_TOK_STR(Minus)
    LOX_TOK_STR(Plus)
    LOX_TOK_STR(Semicolon)
    LOX_TOK_STR(Slash)
    LOX_TOK_STR(Star)

    LOX_TOK_STR(Bang)
    LOX_TOK_STR(BangEqual)
    LOX_TOK_STR(Equal)
    LOX_TOK_STR(EqualEqual)
    LOX_TOK_STR(Greater)
    LOX_TOK_STR(GreaterEqual)
    LOX_TOK_STR(Less)
    LOX_TOK_STR(LessEqual)

    LOX_TOK_STR(Identifier)
    LOX_TOK_STR(String)
    LOX_TOK_STR(Number)

    LOX_TOK_STR(And)
    LOX_TOK_STR(Class)
    LOX_TOK_STR(Else)
    LOX_TOK_STR(False)
    LOX_TOK_STR(Fun)
    LOX_TOK_STR(For)
    LOX_TOK_STR(If)
    LOX_TOK_STR(Nil)
    LOX_TOK_STR(Or)
    LOX_TOK_STR(Print)
    LOX_TOK_STR(Return)
    LOX_TOK_STR(Super)
    LOX_TOK_STR(This)
    LOX_TOK_STR(True)
    LOX_TOK_STR(Var)
    LOX_TOK_STR(While)

    LOX_TOK_STR(EndOfFile)
  }
  return "?";
}
#undef LOX_TOK_STR

class Token {
 public:
  Token(TokenType, uint32_t line);
  static Token of_identifier(std::string_view, uint32_t line);
  static Token of_string(std::string_view, uint32_t line);
  static Token of_number(double, uint32_t line);

  TokenType type() const;
  uint32_t line() const;
  std::string_view get_str() const;
  double get_number() const;

 private:
  TokenType type_;
  uint32_t line_;
  std::unique_ptr<char[]> str_;
  union {
    size_t len_;
    double number_;
  };

  Token() = default;
};

}  // namespace lox

std::ostream& operator<<(std::ostream& os, lox::TokenType t);
std::ostream& operator<<(std::ostream& os, const lox::Token& t);