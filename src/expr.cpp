#include "expr.hpp"

namespace lox {

BinaryExpr::BinaryExpr(const Token& binop,
                       std::unique_ptr<Expr> l,
                       std::unique_ptr<Expr> r)
    : op(&binop), left(std::move(l)), right(std::move(r)) {}

void BinaryExpr::accept(IVisitor& v) const {
  v.visit_binary_expr(*this);
}

GroupingExpr::GroupingExpr(std::unique_ptr<Expr> e)
    : expression(std::move(e)) {}

void GroupingExpr::accept(IVisitor& v) const {
  v.visit_grouping_expr(*this);
}

LiteralExpr::LiteralExpr(const Token& val) : value(&val) {}

void LiteralExpr::accept(IVisitor& v) const {
  v.visit_literal_expr(*this);
}

UnaryExpr::UnaryExpr(const Token& uop, std::unique_ptr<Expr> r)
    : op(&uop), right(std::move(r)) {}

void UnaryExpr::accept(IVisitor& v) const {
  v.visit_unary_expr(*this);
}

}  // namespace lox