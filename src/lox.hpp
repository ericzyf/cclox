#pragma once
#include <string_view>
#include "token.hpp"

namespace lox {

void report(unsigned line, std::string_view where, std::string_view message);
void error(unsigned line, std::string_view message);
void error(const Token&, std::string_view message);

}  // namespace lox