#include "parser.hpp"
#include <cassert>
#include "lox.hpp"

namespace {

class ParseError : public std::exception {};

}  // namespace

namespace lox {

Parser::Parser(const std::vector<Token>& tokens) : current_(tokens.begin()) {
  assert(tokens.back().type() == TokenType::EndOfFile);
}

std::unique_ptr<Expr> Parser::parse() {
  try {
    return expression();
  } catch (...) {
    return nullptr;
  }
}

bool Parser::at_end() const {
  return current_->type() == TokenType::EndOfFile;
}

const Token& Parser::peek() const {
  return *current_;
}

const Token* Parser::match(TokenType t) {
  if (!at_end() && peek().type() == t) {
    return &*current_++;
  }
  return nullptr;
}

const Token* Parser::match(std::initializer_list<TokenType> ts) {
  if (!at_end()) {
    for (auto t : ts) {
      if (peek().type() == t) {
        return &*current_++;
      }
    }
  }
  return nullptr;
}

void Parser::throw_error(std::string_view message) {
  error(peek(), message);
  throw ParseError();
}

void Parser::synchronize() {
  // TODO:
  std::abort();
}

std::unique_ptr<Expr> Parser::expression() {
  return equality();
}

std::unique_ptr<Expr> Parser::equality() {
  auto expr = comparison();
  while (auto op = match({TokenType::BangEqual, TokenType::EqualEqual})) {
    auto e = std::make_unique<BinaryExpr>(*op, std::move(expr), comparison());
    expr = std::move(e);
  }
  return expr;
}

std::unique_ptr<Expr> Parser::comparison() {
  auto expr = term();
  while (auto op = match({TokenType::Greater, TokenType::GreaterEqual,
                          TokenType::Less, TokenType::LessEqual})) {
    auto e = std::make_unique<BinaryExpr>(*op, std::move(expr), term());
    expr = std::move(e);
  }
  return expr;
}

std::unique_ptr<Expr> Parser::term() {
  auto expr = factor();
  while (auto op = match({TokenType::Minus, TokenType::Plus})) {
    auto e = std::make_unique<BinaryExpr>(*op, std::move(expr), factor());
    expr = std::move(e);
  }
  return expr;
}

std::unique_ptr<Expr> Parser::factor() {
  auto expr = unary();
  while (auto op = match({TokenType::Slash, TokenType::Star})) {
    auto e = std::make_unique<BinaryExpr>(*op, std::move(expr), unary());
    expr = std::move(e);
  }
  return expr;
}

std::unique_ptr<Expr> Parser::unary() {
  if (auto op = match({TokenType::Bang, TokenType::Minus})) {
    return std::make_unique<UnaryExpr>(*op, unary());
  }
  return primary();
}

std::unique_ptr<Expr> Parser::primary() {
  if (auto lit = match({TokenType::Number, TokenType::String, TokenType::True,
                        TokenType::False, TokenType::Nil})) {
    return std::make_unique<LiteralExpr>(*lit);
  }

  if (match(TokenType::LeftParen)) {
    auto expr = expression();
    if (match(TokenType::RightParen)) {
      return std::make_unique<GroupingExpr>(std::move(expr));
    }
    throw_error("Expect ')' after expression.");
  }

  throw_error("Expect expression.");
  // unreachable
  return nullptr;
}

}  // namespace lox