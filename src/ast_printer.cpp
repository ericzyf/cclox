#include "ast_printer.hpp"
#include <cassert>

namespace lox {

AstPrinter::AstPrinter(std::ostream& os) : os_(os) {}

void AstPrinter::visit_binary_expr(const BinaryExpr& e) {
  assert(e.op);
  os_ << '(' << e.op->type() << ' ';

  assert(e.left);
  e.left->accept(*this);

  os_ << ' ';

  assert(e.right);
  e.right->accept(*this);

  os_ << ')';
}

void AstPrinter::visit_grouping_expr(const GroupingExpr& e) {
  os_ << "(group ";
  assert(e.expression);
  e.expression->accept(*this);
  os_ << ')';
}

void AstPrinter::visit_literal_expr(const LiteralExpr& e) {
  assert(e.value);
  os_ << *e.value;
}

void AstPrinter::visit_unary_expr(const UnaryExpr& e) {
  assert(e.op);
  os_ << '(' << e.op->type() << ' ';
  assert(e.right);
  e.right->accept(*this);
  os_ << ')';
}

}  // namespace lox