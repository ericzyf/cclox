#pragma once
#include <optional>
#include <vector>
#include "token.hpp"

namespace lox {

class Lexer {
 public:
  explicit Lexer(std::string_view);
  Token next_token();
  unsigned error_count() const;
  static std::vector<Token> tokenize(std::string_view, unsigned&);

 private:
  std::string_view source_;
  size_t start_;
  size_t current_;
  uint32_t line_;
  unsigned error_count_;

  bool at_end() const;
  char peek() const;
  char peek_next() const;
  char advance();
  bool match(char expected);

  void line_comment();
  Token string_literal();
  std::optional<Token> number_literal();
  Token identifier();
};

}  // namespace lox