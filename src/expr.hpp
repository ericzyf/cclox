#pragma once
#include "token.hpp"

namespace lox {

class BinaryExpr;
class GroupingExpr;
class LiteralExpr;
class UnaryExpr;

class Expr {
 public:
  class IVisitor {
   public:
    virtual void visit_binary_expr(const BinaryExpr&) = 0;
    virtual void visit_grouping_expr(const GroupingExpr&) = 0;
    virtual void visit_literal_expr(const LiteralExpr&) = 0;
    virtual void visit_unary_expr(const UnaryExpr&) = 0;
    virtual ~IVisitor() = default;
  };

  virtual void accept(IVisitor&) const = 0;
  virtual ~Expr() = default;
};

class BinaryExpr : public Expr {
 public:
  const Token* op;
  std::unique_ptr<Expr> left;
  std::unique_ptr<Expr> right;

  BinaryExpr(const Token&, std::unique_ptr<Expr>, std::unique_ptr<Expr>);
  void accept(IVisitor&) const override;
};

class GroupingExpr : public Expr {
 public:
  std::unique_ptr<Expr> expression;

  explicit GroupingExpr(std::unique_ptr<Expr>);
  void accept(IVisitor&) const override;
};

class LiteralExpr : public Expr {
 public:
  const Token* value;

  explicit LiteralExpr(const Token&);
  void accept(IVisitor&) const override;
};

class UnaryExpr : public Expr {
 public:
  const Token* op;
  std::unique_ptr<Expr> right;

  UnaryExpr(const Token&, std::unique_ptr<Expr>);
  void accept(IVisitor&) const override;
};

}  // namespace lox