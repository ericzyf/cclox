#pragma once
#include "expr.hpp"

namespace lox {

class AstPrinter : public Expr::IVisitor {
 public:
  explicit AstPrinter(std::ostream&);
  void visit_binary_expr(const BinaryExpr&) override;
  void visit_grouping_expr(const GroupingExpr&) override;
  void visit_literal_expr(const LiteralExpr&) override;
  void visit_unary_expr(const UnaryExpr&) override;

 private:
  std::ostream& os_;
};

}  // namespace lox