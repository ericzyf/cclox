#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include "ast_printer.hpp"
#include "lexer.hpp"
#include "parser.hpp"

namespace {

bool run(std::string_view source) {
  using namespace lox;

  unsigned ec = 0;
  const auto tokens = Lexer::tokenize(source, ec);
  if (ec) {
    return false;
  }

  const auto ast = Parser{tokens}.parse();
  if (!ast) {
    return false;
  }

  AstPrinter ap(std::cout);
  ast->accept(ap);
  std::cout << std::endl;

  return true;
}

void run_prompt() {
  for (std::string line;;) {
    std::cout << "> " << std::flush;
    if (std::getline(std::cin, line)) {
      if (!line.empty()) {
        run(line);
      }
    } else {
      return;
    }
  }
}

std::string read_file_to_str(const char* path) {
  if (!path) {
    throw std::ios_base::failure("path is nullptr");
  }

  const char* msg = nullptr;
  const auto file_sz = std::filesystem::file_size(path);
  if (std::ifstream ifs{path, std::ios::binary}) {
    std::string s(file_sz, '\0');
    if (ifs.read(s.data(), file_sz)) {
      return s;
    } else {
      msg = "Error occurred while reading file";
    }
  } else {
    msg = "Failed to open file";
  }
  throw std::ios_base::failure(msg);
}

void run_file(const char* path) {
  if (!run(read_file_to_str(path))) {
    std::exit(65);
  }
}

}  // namespace

int main(int argc, char** argv) {
  switch (argc) {
    case 1:
      run_prompt();
      break;

    case 2:
      run_file(argv[1]);
      break;

    default:
      std::cout << "Usage: jlox [script]\n";
      return 64;
  }
}
