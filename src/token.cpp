#include "token.hpp"
#include <cassert>
#include <cstring>

namespace {

// Returns an owned char array which is a duplicate of the string |s|. This
// newly allocated char array is also null-terminated so that it can be safely
// turned into a C-string (by calling get()/release()) when needed.
// If |s| is an empty string, the returned char array consists of only one '\0'.
std::unique_ptr<char[]> to_boxed_str(std::string_view s) {
  const auto len = s.size();
  std::unique_ptr<char[]> str(new char[len + 1]);
  if (len > 0) {
    std::memcpy(str.get(), s.data(), len);
  }
  str[len] = '\0';
  return str;
}

}  // namespace

namespace lox {

Token::Token(TokenType t, uint32_t line) : type_(t), line_(line) {
  // TokenType must not be 'Literals' that have additional values.
  assert(t != TokenType::Identifier);
  assert(t != TokenType::String);
  assert(t != TokenType::Number);
}

Token Token::of_identifier(std::string_view s, uint32_t line) {
  Token tok;
  tok.type_ = TokenType::Identifier;
  tok.line_ = line;
  tok.str_ = to_boxed_str(s);
  tok.len_ = s.size();
  return tok;
}

Token Token::of_string(std::string_view s, uint32_t line) {
  Token tok;
  tok.type_ = TokenType::String;
  tok.line_ = line;
  tok.str_ = to_boxed_str(s);
  tok.len_ = s.size();
  return tok;
}

Token Token::of_number(double n, uint32_t line) {
  Token tok;
  tok.type_ = TokenType::Number;
  tok.line_ = line;
  tok.number_ = n;
  return tok;
}

TokenType Token::type() const {
  return type_;
}

uint32_t Token::line() const {
  return line_;
}

std::string_view Token::get_str() const {
  assert(type_ == TokenType::Identifier || type_ == TokenType::String);
  return {str_.get(), len_};
}

double Token::get_number() const {
  assert(type_ == TokenType::Number);
  return number_;
}

}  // namespace lox

std::ostream& operator<<(std::ostream& os, lox::TokenType t) {
  return os << lox::to_str(t);
}

std::ostream& operator<<(std::ostream& os, const lox::Token& t) {
  os << t.type();
  // For literals, print their values.
  switch (t.type()) {
    case lox::TokenType::Identifier:
    case lox::TokenType::String:
      os << '(' << t.get_str() << ')';
      break;

    case lox::TokenType::Number:
      os << '(' << t.get_number() << ')';
      break;

    default:
      break;
  }
  return os << "@L" << t.line();
}